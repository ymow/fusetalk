// Dependencies
var express = require('express'),
     OpenTok = require('opentok');
    // OpenTok = require('../../lib/opentok');

// var server_port = process.env.OPENSHIFT_NODEJS_PORT || 3000
// var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1'
 var server_port = process.env.PORT || 3000;


// Verify that the API Key and API Secret are defined
var apiKey = "45091852",
    apiSecret = "b9e0dce42c62cb4603ef6a290b18c67874848ce4";
if (!apiKey || !apiSecret) {
  console.log('You must specify API_KEY and API_SECRET environment variables');
  process.exit(1);
}

// Initialize the express app
var app = express();
// app.use(express.static(__dirname + '/public'));

// Initialize OpenTok
var opentok = new OpenTok(apiKey, apiSecret);

// Create a session and store it in the express app
opentok.createSession({ mediaMode: 'routed' },function(err, session) {
  if (err) throw err;
  app.set('sessionId', session.sessionId);
  // We will wait on starting the app until this is done
  init();
});

app.get('/', function(req, res) {
  res.render('index.ejs');
});

app.get('/host', function(req, res) {
  var sessionId = app.get('sessionId'),
      // generate a fresh token for this client
      token = opentok.generateToken(sessionId, { role: 'moderator' });

  res.status(200).json( {
    apiKey: apiKey,
    sessionId: sessionId,
    token: token
  });
    // res.status(503).json('503,oh noes!');
    // res.json(503, 'oh noes 503!');
    // res.json(404, 'I dont have that');
});

app.get('/participant', function(req, res) {
  var sessionId = app.get('sessionId'),
      // generate a fresh token for this client
      token = opentok.generateToken(sessionId, { role: 'moderator' });

  res.json( {
    apiKey: apiKey,
    sessionId: sessionId,
    token: token
  });
});

app.get('/history', function(req, res) {
  var page = req.param('page') || 1,
      offset = (page - 1) * 5;
  opentok.listArchives({ offset: offset, count: 5 }, function(err, archives, count) {
    if (err) return res.send(500, 'Could not list archives. error=' + err.message);
    res.json( {
      archives: archives,
      showPrevious: page > 1 ? ('/history?page='+(page-1)) : null,
      showNext: (count > offset + 5) ? ('/history?page='+(page+1)) : null
    });
  });
});

app.get('/download/:archiveId', function(req, res) {
  var archiveId = req.param('archiveId');
  opentok.getArchive(archiveId, function(err, archive) {
    if (err) return res.send(500, 'Could not get archive '+archiveId+'. error='+err.message);
    res.redirect(archive.url);
  });
});

app.get('/start', function(req, res) {
  opentok.startArchive(app.get('sessionId'), {
    name: 'Node Archiving Sample App'
  }, function(err, archive) {
    if (err) return res.send(500,
      'Could not start archive for session '+sessionId+'. error='+err.message
    );
    res.json(archive);
  });
});

app.get('/stop/:archiveId', function(req, res) {
  var archiveId = req.param('archiveId');
  opentok.stopArchive(archiveId, function(err, archive) {
    if (err) return res.send(500, 'Could not stop archive '+archiveId+'. error='+err.message);
    res.json(archive);
  });
});

app.get('/delete/:archiveId', function(req, res) {
  var archiveId = req.param('archiveId');
  opentok.deleteArchive(archiveId, function(err) {
    if (err) return res.send(500, 'Could not stop archive '+archiveId+'. error='+err.message);
    res.redirect('/history');
  });
});

// Start the express app
function init() {
  app.listen(server_port, function() {
    // console.log('You\'re app is now ready at http://localhost:3000/');
     console.log( "Listening on "  + ", server_port " + server_port )
  });
}
